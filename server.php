<?php
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

require __DIR__ . '/vendor/autoload.php';

$app = new App();

$container = $app->getContainer();
$container['upload_directory'] = __DIR__ . '/uploads';

$app->post('/', function(Request $request, Response $response) {
    $directory = $this->get('upload_directory');

    $uploadedFiles = $request->getUploadedFiles();

    // handle single input with single file upload
    $uploadedFile = $uploadedFiles['file'];
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $filename = moveUploadedFile($directory, $uploadedFile);
    }
    return $response->withRedirect('http://localhost:8888/', 301);
});

$app->get('/', function(Request $request, Response $response){
    $directory = $this->get('upload_directory');

    $filesInDirectory = glob($directory."/*");
    $filesToSend = [];

    foreach($filesInDirectory as $fileName){
        array_push($filesToSend,basename($fileName));
    }
    $response->getBody()->write(json_encode($filesToSend));
    return $response;
});

$app->delete('/{fn}', function(Request $request, Response $response, array $args){
    $directory = $this->get('upload_directory');
    unlink($directory . DIRECTORY_SEPARATOR  . $args["fn"]);
    return $response->withStatus(201,"ECHO BITE");
});

/**
 * Moves the uploaded file to the upload directory and assigns it a unique name
 * to avoid overwriting an existing uploaded file.
 *
 * @param string $directory directory to which the file is moved
 * @param UploadedFile $uploadedFile file uploaded file to move
 * @return string filename of moved file
 */
function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    $filename = $uploadedFile->getClientFilename();
    if (pathinfo($filename, PATHINFO_EXTENSION) === "pdf") {
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    }
    
    return $filename;
}

$app->run();